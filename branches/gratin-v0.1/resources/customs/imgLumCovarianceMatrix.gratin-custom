<gratin file="customNode">
 <string val="imgLumCovarianceMatrix" name="customHandleName"/>
 <string val="statistics/matrices/" name="customHandlePath"/>
 <string val="Compute the covariance matrix of the input intensity for each pixel" name="customHandleDesc"/>
 <string val="Compute the covariance matrix of the input colored image for each pixel. &lt;br>&#xa;&lt;br>&#xa;The covariance matrix of the input luminance is computed over a neighborhood and weighted by a spatial gaussian (controled by the &quot;halfsize&quot; parameter as well as by the alpha value of the input image. The luminance is computed as the length of the input 3D color.&lt;br>&#xa;&lt;br>&#xa;The covariance matrix is a 2x2 symatric matrix:&lt;br> &#xa;| Cxx | Cxy |&lt;br>&#xa; | Cxy | Cyy |&lt;br>&#xa;The result is stored as (Cxx,Cyy,Cxy,alpha).&lt;br>" name="customHandleHelp"/>
 <int val="1" name="customHandleInputDescSize"/>
 <int val="1" name="customHandleOutputDescSize"/>
 <string val="img" name="customHandleInputDesc-0"/>
 <string val="lumCovMatrix" name="customHandleOutputDesc-0"/>
 <string val="imgGeneric" name="origHandleName"/>
 <string val="generic/" name="origHandlePath"/>
 <string val="Generic node for image processing" name="origHandleDesc"/>
 <string val="This node was designed to create custom shaders&#xa;The settings widget allows to modify input, output parameters&#xa;as well as the output image sizes.&#xa;&#xa;WARNING1: you will be able to modify the number of inputs/outputs&#xa;only if the node is entirely disconnected.&#xa;WARNING2: input/output names will not be modified if a custom node is created&#xa;from this node beacause these names should be edited directly in the setting widget&#xa;WARNING3: modifying the settings of a node will remove all previously&#xa;defined keyframes&#xa;&#xa;* Special data allows to modify the output image (using a multiple of&#xa;the input size, plus an offset vector&#xa;Check use mouse if you want to interact with the mouse&#xa;&#xa;* Input data allows to add/remove/displace input data (only if disconnected)&#xa;&#xa;* Output data allows to add/remove/displace output data (only if disconnected)&#xa;&#xa;* Param data allows to add/remove parameters that will automatically be &#xa;included in your shader.&#xa;For each of them you may choose a proper name, type, and min/max/default values.&#xa;Check keyframes if you want your parameter to be accessible&#xa; through the animation widget&#xa;&#xa;In the node interface itself:&#xa;Source tab contains the head and body of your GLSL source (only the body is editable)&#xa;Params tab contains the defined widgets according to your setting choices&#xa;Log tab contains compilation errors&#xa;&#xa;Once your settings and source completed, click on apply to see the result&#xa;" name="origHandleHelp"/>
 <int val="1" name="origHandleInputDescSize"/>
 <int val="1" name="origHandleOutputDescSize"/>
 <string val="img" name="origHandleInputDesc-0"/>
 <string val="lumCovMatrix" name="origHandleOutputDesc-0"/>
 <string val="imgGeneric" name="nodeName-0"/>
 <string val="generic/" name="nodePath-0"/>
 <uint val="21" name="nodeId-0"/>
 <uint val="1" name="nodeGraphId-0"/>
 <uint val="1" name="nbInputs-0"/>
 <uint val="1" name="nbOutputs-0"/>
 <vector2f name="nodePos-0">
  <float val="1169.89" name="0"/>
  <float val="-4612.67" name="1"/>
 </vector2f>
 <bool val="0" name="nodeGrouped-0"/>
 <bool val="1" name="nodeHasWidget-0"/>
 <IOData name="node-0">
  <string val="img" name="genericIName-0"/>
  <string val="lumCovMatrix" name="genericOName-0"/>
  <vector2f name="genericTSize">
   <float val="1" name="0"/>
   <float val="1" name="1"/>
  </vector2f>
  <vector2f name="genericTOffset">
   <float val="0" name="0"/>
   <float val="0" name="1"/>
  </vector2f>
  <bool val="0" name="genericNeedMouse"/>
  <uint val="0" name="genericNbSliderFloat"/>
  <uint val="1" name="genericNbSliderInt"/>
  <bool val="1" name="genericSliderIntIsKF-0"/>
  <string val="halfsize" name="genericSliderIntName-0"/>
  <string val="#version 330&#xa;&#xa;in vec2 texcoord;&#xa;&#xa;layout(location = 0) out vec4 lumCovMatrix;&#xa;uniform sampler2D img;&#xa;uniform int halfsize;&#xa;" name="genericHead"/>
  <string val="#define F 0.619928135&#xa;&#xa;float sigmaS = float(halfsize)*F;&#xa;int size = halfsize*2+1;&#xa;float sumW = 0.0;&#xa;vec2 meanG = vec2(0.0);&#xa;vec2 pixelSize = 1.0/vec2(textureSize(img,0));&#xa;float facS = -1.0/(2.0*sigmaS*sigmaS);&#xa;&#xa;float weight(in vec4 col) {&#xa;&#x9;// luminance * alpha&#xa;&#x9;return length(col.xyz)*col.w;&#xa;}&#xa;&#xa;void loadVals() {&#xa;&#xa;&#x9;int k=0;&#xa;&#xa;&#x9;for(int i=-halfsize;i&lt;=halfsize;++i) {&#xa;&#x9;&#x9;for(int j=-halfsize;j&lt;=halfsize;++j) {&#xa;&#x9;&#x9;&#x9;vec2 pos = vec2(float(i),float(j));&#xa;&#x9;&#x9;&#x9;vec4 col = texture(img,texcoord+pos*pixelSize);&#xa;&#x9;&#x9;&#x9;&#xa;&#x9;&#x9;&#x9;float distS = length(pos);&#xa;&#x9;&#x9;&#x9;float wS = exp(facS*float(distS*distS));&#xa;&#x9;&#x9;&#x9;float w = wS*weight(col);&#xa;&#xa;&#x9;&#x9;&#x9;sumW += w;&#xa;&#x9;&#x9;&#x9;meanG += pos*w;&#xa;&#x9;&#x9;&#x9;k++;&#xa;&#x9;&#x9;}&#xa;&#x9;}&#x9;&#xa;&#xa;&#x9;meanG /= sumW;&#xa;}&#xa;&#xa;vec3 covarianceMatrix() {&#xa;&#x9;vec3 cov = vec3(0.0);&#xa;&#xa;&#x9;for(int i=-halfsize;i&lt;=halfsize;++i) {&#xa;&#x9;&#x9;for(int j=-halfsize;j&lt;=halfsize;++j) {&#xa;&#x9;&#x9;&#x9;vec2 pos = vec2(float(i),float(j));&#xa;&#x9;&#x9;&#x9;vec4 col = texture(img,texcoord+pos*pixelSize);&#xa;&#x9;&#x9;&#xa;&#x9;&#x9;&#x9;float distS = length(pos);&#xa;&#x9;&#x9;&#x9;float wS = exp(facS*float(distS*distS));&#xa;&#x9;&#x9;&#x9;float w = wS*weight(col);&#xa;&#xa;&#x9;&#x9;vec3 v = vec3(pos.xy-meanG,w/sumW);&#xa;&#x9;&#xa;&#x9;&#x9;cov.x += v.z*v.x*v.x;&#xa;&#x9;&#x9;cov.y += v.z*v.y*v.y;&#xa;&#x9;&#x9;cov.z += v.z*v.x*v.y;&#xa;&#x9;&#x9;}&#xa;&#x9;}&#xa;&#xa;&#x9;return cov;&#xa;}&#xa;&#xa;void main() {&#xa;&#x9;vec4 d = texture(img,texcoord);&#xa;&#xa;&#x9;if(d==vec4(0.0)) {&#xa;&#x9;&#x9;lumCovMatrix = vec4(0);&#xa;&#x9;&#x9;return;&#xa;&#x9;}&#xa;&#xa;&#x9;loadVals();&#xa;&#x9;vec3 cov = covarianceMatrix();&#xa;&#x9;float alpha = texture(img,texcoord).w;&#xa;&#x9;lumCovMatrix = vec4(cov,d.w);&#xa;}&#xa;" name="genericBody"/>
 </IOData>
 <IOData name="nodeWidget-0">
  <uint val="1" name="nbChildren"/>
  <IOData name="childWidget-0">
   <IOData name="keyframedFloat">
    <float val="1" name="minVal"/>
    <float val="49" name="maxVal"/>
    <float val="1" name="currentVal"/>
    <IOData name="curve">
     <int val="0" name="interpType"/>
     <int val="0" name="leftType"/>
     <int val="0" name="rightType"/>
     <IOData name="interpolator">
      <uint val="1" name="nbPoints"/>
      <uint val="0" name="nbTangents"/>
      <vector2f name="controlPoint-0">
       <float val="0" name="0"/>
       <float val="1" name="1"/>
      </vector2f>
     </IOData>
    </IOData>
   </IOData>
   <int val="1" name="sliderVal"/>
   <int val="1" name="sliderMinVal"/>
   <int val="49" name="sliderMaxVal"/>
   <uint val="0" name="nbChildren"/>
  </IOData>
 </IOData>
 <vector2f name="nodeInputPos-0-0">
  <float val="56.32" name="0"/>
  <float val="28.16" name="1"/>
 </vector2f>
 <vector2f name="nodeOutputPos-0-0">
  <float val="61.952" name="0"/>
  <float val="30.976" name="1"/>
 </vector2f>
</gratin>
