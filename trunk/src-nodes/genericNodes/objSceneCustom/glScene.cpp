#include "glScene.h"
#include "core/textureIO.h"

using namespace std;

GlMaterial::GlMaterial(const Vector3f &ka,
		       const Vector3f &kd,
		       const Vector3f &ks,
		       float ns,
		       const std::string &pathKaMap,
		       const std::string &pathKdMap,
		       const std::string &pathKsMap,
		       const std::string &pathNsMap,
		       const std::string &pathBumpMap)
  : _ka(ka), _kd(kd), _ks(ks), _ns(ns),
    _mapKa(NULL), _mapKd(NULL), _mapKs(NULL), _mapNs(NULL), _mapBump(NULL) {

  TextureIO tio;
  _mapKa   = tio.load(QString(pathKaMap.c_str()));
  _mapKd   = tio.load(QString(pathKdMap.c_str()));
  _mapKs   = tio.load(QString(pathKsMap.c_str()));
  _mapNs   = tio.load(QString(pathNsMap.c_str()));
  _mapBump = tio.load(QString(pathBumpMap.c_str()));
}

GlMaterial::~GlMaterial() {
  delete _mapKa;
  delete _mapKd;
  delete _mapKs;
  delete _mapNs;
  delete _mapBump;
}

GlScene::GlScene(const std::string &filename) : _vao(NULL) {
  ObjMesh *m = ObjMesh::LoadFromFile(filename);

  if(m) {
    ObjMesh *s = m->createIndexedFaceSet();
    delete m;
    
    if(s) {
      s->computeTangents();
      createVAO(s);
      createGlMaterials(s);
      computeSceneProperties(s);
    }

    delete s;
  }
}

GlScene::~GlScene() {
  deleteVAO();
  deleteGlMaterials();
}

void GlScene::createVAO(ObjMesh *m) {
  // check if the mesh exists and is not empty
  if(!m || m->positions.empty()) {
    cout << "Error while creating Mesh VAO: no vertices available!\n";
    return;
  }

  // check if faces are triangles only
  for(unsigned int i=0;i<m->getNofSubMeshes();++i) {
    if(m->getSubMesh(i)->getConstNofVerticesPerFace()!=3) {
      cout << "Error while creating Mesh VAO: faces are not triangles only!\n";
      return;
    }
  }

  std::vector<Vector3i > faces;
  createIndexArray(m,faces);
  deleteVAO();

  Vector3f defaultAttrib(0.0f,0.0f,0.0f);

  _vao = new VertexarrayObject();
  _vao->addAttrib(m->positions.size()*sizeof(Vector3f),m->positions[0].data(),3);

  if(!m->normals.empty())
    _vao->addAttrib(m->normals.size()*sizeof(Vector3f),m->normals[0].data(),3);
  else
    _vao->addAttrib(sizeof(Vector3f),defaultAttrib.data(),3);

  if(!m->tangents.empty())
    _vao->addAttrib(m->tangents.size()*sizeof(Vector3f),m->tangents[0].data(),3);
  else
    _vao->addAttrib(sizeof(Vector3f),defaultAttrib.data(),3);

  if(!m->texcoords.empty())
    _vao->addAttrib(m->texcoords.size()*sizeof(Vector2f),m->texcoords[0].data(),2);
  else
    _vao->addAttrib(sizeof(Vector2f),defaultAttrib.data(),2);

  _vao->setIndices(faces.size()*sizeof(Vector3i),faces[0].data());
}

void GlScene::createIndexArray(ObjMesh *m,std::vector<Vector3i > &faces) {
  faces.clear();
  _subMeshIds.clear();
  
  int currentStart = 0;
  int currentEnd = 0;

  for(unsigned int i=0;i<m->getNofSubMeshes();++i) {
    
    const ObjSubMesh *sm = m->getSubMesh(i);

    for(unsigned int j=0;j<sm->getNofFaces();++j) {
      ObjConstFaceHandle f = sm->getFace(j);

      faces.push_back(Vector3i(f.vertexId(0),f.vertexId(1),f.vertexId(2)));
      
      currentEnd += 3;
    }

    _subMeshIds.push_back(Vector2i(currentStart,currentEnd-1));
    currentStart = currentEnd;
  }
}

void GlScene::deleteGlMaterials() {
  _matIds.clear();
  for(unsigned int i=0;i<_mats.size();++i) {
    delete _mats[i];
  }
  _mats.clear();
}

void GlScene::createGlMaterials(ObjMesh *m) {
  deleteGlMaterials();
  
  for(unsigned int i=0;i<m->getNofSubMeshes();++i) {
    _matIds.push_back(m->getSubMesh(i)->getMaterialId());
  }

  for(unsigned int i=0;i<m->getNofMaterials();++i) {
    const ObjMaterial *mat = m->getMaterial(i);
    GlMaterial *gm = new GlMaterial(mat->getKa(),mat->getKd(),mat->getKs(),mat->getNs(),
				    mat->getMapKa(),mat->getMapKd(),mat->getMapKs(),
				    mat->getMapNs(),mat->getMapBump());
    _mats.push_back(gm);
  }

  bool defaultMaterialCreated = false;
  for(unsigned int i=0;i<_matIds.size();++i) {
    if(_matIds[i]<0) {
      if(!defaultMaterialCreated) {
	_mats.push_back(new GlMaterial());
	defaultMaterialCreated = true;
      }

      _matIds[i] = (int)_mats.size()-1;
    }
  }
}

void GlScene::computeSceneProperties(ObjMesh *m) {
  if(m->positions.empty()) {
    _barycenter = Vector3f(0.0f,0.0f,0.0f);
    _radius = 1.0f;
    return;
  }

  Vector3f minv(m->positions[0]);
  Vector3f maxv(m->positions[0]);
  Vector3f bar(0.0f,0.0f,0.0f);

  for(unsigned int i=0;i<m->positions.size();++i) {
    bar += m->positions[i];
    for(unsigned int j=0;j<3;++j) {
      if(m->positions[i][j]>maxv[j]) maxv[j] = m->positions[i][j];
      if(m->positions[i][j]<minv[j]) minv[j] = m->positions[i][j];
    }
  }

  Vector3f med = maxv-minv;
  _barycenter = bar/(float)m->positions.size();
  _radius = med.norm()/2.0f;

  // cout << "Scene properties: (" << _barycenter[0] << "," << _barycenter[1] << "," << _barycenter[2] << " -- " << _radius << endl;
  // cout << "Nb positions : " << m->positions.size() << endl;
  // cout << "Nb normals : " << m->normals.size() << endl;
  // cout << "Nb meshes : " << nbMeshes() << endl;
  // for(unsigned int i=0;i<nbMeshes();++i) {
  //   cout << "\t" << _subMeshIds[i][0] << "," << _subMeshIds[i][1] << endl;
  // }
}
