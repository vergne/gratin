// This file is part of Gratin, a programmable Node-based System 
// for GPU-friendly Applications.
//
// Copyright (C) 2013-2014 Romain Vergne <romain.vergne@inria.fr>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "objSceneCustom.h"
#include <QString>
#include <QDebug>
#include <QStringList>
#include <QFileDialog>
#include <iostream>

#include "misc/extinclude.h"
#include "misc/glutils.h"
#include "core/pbgraph.h"
#include "misc/mesh.h"
#include "misc/objLoader.h"

using namespace std;

QDir ObjSceneCustomWidget::_currentPath = QDir::currentPath();

ObjSceneCustomWidget::ObjSceneCustomWidget(ObjSceneCustomNode *node)
  : GenericCustomWidget(node),
    _load(new QPushButton("Load...")),
    _default(new QPushButton("Reset cam")) {

  _camWidget = new TrackballCameraWidget(node,"Camera",node->camera());

  // default parameters 
  userLayout()->addWidget(_load);
  userLayout()->addWidget(_camWidget);
  userLayout()->addWidget(_default);

  connect(_load,SIGNAL(clicked()),this,SLOT(loadClicked()));
  connect(_default,SIGNAL(clicked()),this,SLOT(defaultClicked()));

  addChildWidget(_camWidget);
}

void ObjSceneCustomWidget::loadClicked() {
  QString filename = QFileDialog::getOpenFileName(0,"Load object",_currentPath.absolutePath(),"Objects (*.obj);;All (*.*)");

  if(filename.isEmpty()) {
    return;
  }

  QDir d(".");
  _currentPath = d.filePath(filename);

  ((ObjSceneCustomNode *)node())->loadObject(filename);
  updateGraph();
}

void ObjSceneCustomWidget::defaultClicked() {
  ((ObjSceneCustomNode *)node())->initCam();
  updateGraph();
}

ObjSceneCustomNode::ObjSceneCustomNode(PbGraph *parent,NodeHandle *handle)
  : GenericCustomNode(true,false,false,true,parent,handle,true,false,Vector2f(0.0f,0.0f),Vector2f(512.0f,512.0f)),
    _filename(GRATIN_APPLI_DATA_PATH+"/objs/sphere.obj"),
    _camera(new TrackballCamera(Vector2i(512,512),Vector3f(0,0,0),1.0f,0)),
    _depth(NULL),
    _scene(NULL) {

  _w = new ObjSceneCustomWidget(this);

  initShaderSource();
  loadObject(_filename);

  _w->updateWidget();
}

ObjSceneCustomNode::~ObjSceneCustomNode() {
  delete _camera;
  delete _depth;
  delete _scene;
}

void ObjSceneCustomNode::apply() {
  
  Glutils::setViewport(outputTex(0)->w(),outputTex(0)->h());
  setOutputParams();

  _fbo.bind();
  _glf->glDrawBuffers(nbOutputs(),buffersOfOutputTex(0));

  initOpenGLState();

  enableShaders();
  _p->setUniformMatrix4fv("model",(GLfloat *)_camera->modelMatrix().data());
  _p->setUniformMatrix4fv("view",(GLfloat *)_camera->viewMatrix().data());
  _p->setUniformMatrix4fv("proj",(GLfloat *)_camera->projMatrix().data());
  _p->setUniform1f("zmin",_camera->zmin());
  _p->setUniform1f("zmax",_camera->zmax());

  bool tessel = useTesselation();
  for(unsigned int i=0;i<_scene->nbMeshes();++i) {
    GlMaterial *m = _scene->material(i);
    _p->setUniform1i("objectID",i);
    _p->setUniform3fv("ka",m->ka().data());
    _p->setUniform3fv("kd",m->kd().data());
    _p->setUniform3fv("ks",m->ks().data());
    _p->setUniform1f("ns",m->ns());

    if(m->hasKaMap()) {
      _p->setUniform1i("hasKaMap",true);
      _p->setUniformTexture("kaMap",GL_TEXTURE_2D,m->kaMap()->id());
    } else {
      _p->setUniform1i("hasKaMap",false);
    }

    if(m->hasKdMap()) {
      _p->setUniform1i("hasKdMap",true);
      _p->setUniformTexture("kdMap",GL_TEXTURE_2D,m->kdMap()->id());
    } else {
      _p->setUniform1i("hasKdMap",false);
    }
    
    if(m->hasKsMap()) {
      _p->setUniform1i("hasKsMap",true);
      _p->setUniformTexture("ksMap",GL_TEXTURE_2D,m->ksMap()->id());
    } else {
      _p->setUniform1i("hasKsMap",false);
    }

    if(m->hasNsMap()) {
      _p->setUniform1i("hasNsMap",true);
      _p->setUniformTexture("nsMap",GL_TEXTURE_2D,m->nsMap()->id());
    } else {
      _p->setUniform1i("hasNsMap",false);
    }

    if(m->hasBumpMap()) {
      _p->setUniform1i("hasBumpMap",true);
      _p->setUniformTexture("bumpMap",GL_TEXTURE_2D,m->bumpMap()->id());
    } else {
      _p->setUniform1i("hasBumpMap",false);
    }
    
    _scene->drawVAO(i,tessel);
  }
  
  FramebufferObject::unbind();

  disableShaders();
  cleanOpenGLState();
}

void ObjSceneCustomNode::loadObject(const QString &filename) {
  QString f = filename;
  QDir d(".");
  f = d.absoluteFilePath(f);

  delete _scene; _scene = NULL;
  makeCurrent();
  _scene = new GlScene(f.toStdString());
  _camera->setSceneParams(_scene->barycenter(),_scene->radius());
  if(_scene)
    _filename = f;
}

void ObjSceneCustomNode::initCam() {
  Vector2i size(outputSize()[0],outputSize()[1]);
  _camera->reinit(size,_camera->sceneCenter(),_camera->sceneRadius());
}

void ObjSceneCustomNode::mousePressEvent(const Vector2f &p,QMouseEvent *me) {
  if(me->button()==Qt::LeftButton) {
    _camera->initRotation(p);
  } else if(me->button()==Qt::RightButton) {
    _camera->initMoveXY(p);
  } else if(me->button()==Qt::MidButton) {
    _camera->initMoveZ(p);
  }

  GenericCustomNode::mousePressEvent(p,me);
}

void ObjSceneCustomNode::mouseMoveEvent(const Vector2f &p,QMouseEvent *me) {
  _camera->move(p);
  GenericCustomNode::mouseMoveEvent(p,me);
}

void ObjSceneCustomNode::wheelEvent(const Vector2f &p,QWheelEvent *we) {
  GenericCustomNode::wheelEvent(p,we);

  const Vector2f v(0.0f,60.0f);

  _camera->initMoveZ(p);
  if(we->delta()>0) {
    _camera->move(p+v);
  } else {
    _camera->move(p-v);
  }
  update();
}

void ObjSceneCustomNode::initFBO() {  
  NodeTexture2D::initFBO();

  _depth = new FloatTexture2D(TextureFormat(GL_TEXTURE_2D,outputSize()[0],outputSize()[1],
  					    GL_DEPTH_COMPONENT24,GL_DEPTH_COMPONENT,GL_FLOAT),
  			      TextureParams(GL_NEAREST,GL_NEAREST));

  _fbo.bind();
  _fbo.attachTexture(GL_TEXTURE_2D,_depth->id(),GL_DEPTH_ATTACHMENT);
  _fbo.isValid();

  FramebufferObject::unbind();
}

void ObjSceneCustomNode::cleanFBO() {
  NodeTexture2D::cleanFBO();
  delete _depth; _depth = NULL;
}

const QString ObjSceneCustomNode::constantVertHead()  {
  return QObject::tr("layout(location = 0) in vec3 inVertex;\n"
		     "layout(location = 1) in vec3 inNormal;\n"
		     "layout(location = 2) in vec3 inTangent;\n"
		     "layout(location = 3) in vec2 inTexcoord;\n"
		     );
}

const QString ObjSceneCustomNode::defaultVertBody()   {
  return QObject::tr("out vec3  normalV;\n"
		     "out float depthV;\n\n"
		     "void main() {\n"
		     "\tmat4 mdv    = view*model;\n"
		     "\tmat4 mvp    = proj*mdv;\n\n"
		     "\tnormalV     = (mdv*vec4(inNormal,0)).xyz;\n"
		     "\tdepthV      = (clamp(-(mdv*vec4(inVertex,1)).z,zmin,zmax)-zmin)/(zmax-zmin);\n"
		     "\tgl_Position = mvp*vec4(inVertex,1);\n"
		     "}\n");
}

const QString ObjSceneCustomNode::constantFragHead()  {
  return QString();
}

const QString ObjSceneCustomNode::defaultFragBody()   {
  return QObject::tr("in vec3  normalV;\n"
		     "in float depthV;\n\n"
		     "void main() {\n"
		     "\toutBuffer0 = vec4(normalize(normalV),depthV);\n"
		     "}\n");
}

const QString ObjSceneCustomNode::constantTessCHead() {
  return QObject::tr("layout(vertices = 3) out;\n");
}

const QString ObjSceneCustomNode::defaultTessCBody()  {
return QObject::tr("void main(void) {\n"
		   "\tgl_TessLevelOuter[0] = 1;\n"
		   "\tgl_TessLevelOuter[1] = 1;\n"
		   "\tgl_TessLevelOuter[2] = 1;\n"
		   "\tgl_TessLevelOuter[3] = 1;\n"
		   "\tgl_TessLevelInner[0] = 1;\n"
		   "\tgl_TessLevelInner[1] = 1;\n"
		   "\tgl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;\n"
		   "}\n");
}

const QString ObjSceneCustomNode::constantTessEHead() {
  return QString();
}

const QString ObjSceneCustomNode::defaultTessEBody()  {
  return QObject::tr("layout(triangles,equal_spacing,ccw) in;\n\n"
  		     "void main() {\n"
  		     "\tvec4 p1 = gl_TessCoord.x*gl_in[0].gl_Position;\n"
  		     "\tvec4 p2 = gl_TessCoord.y*gl_in[1].gl_Position;\n"
  		     "\tvec4 p3 = gl_TessCoord.z*gl_in[2].gl_Position;\n"
  		     "\tgl_Position = p1+p2+p3;\n"
  		     "}\n");
}

const QString ObjSceneCustomNode::constantGeomHead()  {
  return QObject::tr("layout(triangles) in;\n");
}

const QString ObjSceneCustomNode::defaultGeomBody()   {
  return QObject::tr("layout(triangle_strip, max_vertices = 3) out;\n\n"
		     "void main() {\n"
		     "\tgl_Position = gl_in[0].gl_Position; EmitVertex();\n"
		     "\tgl_Position = gl_in[1].gl_Position; EmitVertex();\n"
		     "\tgl_Position = gl_in[2].gl_Position; EmitVertex();\n"
		     "\tEndPrimitive();\n"
		     "}\n");
}

bool ObjSceneCustomNode::save(SceneSaver  *saver) {
  bool ret = true;

  // obj file and camera 
  QString name = saver->absoluteToRelativePath(_filename);
  if(!saver->saveString("filename",name))      ret = false;
  if(!saver->saveIOData("camera",_camera))     ret = false;
  if(!GenericCustomNode::save(saver))          ret = false;

  return ret;
}

bool ObjSceneCustomNode::load(SceneLoader *loader) {
  bool ret = true;

  _filename = GRATIN_APPLI_DATA_PATH+"/objs/sphere.obj";
  QString name;
  if(!loader->loadString("filename",name)) ret = false;
  
  if(ret) {
    _filename = loader->relativeToAbsolutePath(name);
  }
  
  if(!loader->loadIOData("camera",_camera))     ret = false;
  loadObject(_filename);  
  if(!GenericCustomNode::load(loader))          ret = false;

  return ret;
}
