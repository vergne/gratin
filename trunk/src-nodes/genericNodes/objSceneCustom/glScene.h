#include "misc/glcontext.h"
#include "misc/vertexarrayObject.h"
#include "misc/texture2D.h"

#include "ObjFormat.h"
#include <string>
#include <vector>

#ifndef GL_SCENE
#define GL_SCENE

class GlMaterial : public GlContext {
  // simplified Phong material 
public:
  GlMaterial(const Vector3f &ka = Vector3f(0.0f,0.0f,0.0f),
	     const Vector3f &kd = Vector3f(0.8f,0.8f,0.8f),
	     const Vector3f &ks = Vector3f(1.0f,1.0f,1.0f),
	     float ns = 50.0f,
	     const std::string &pathKaMap = "",
	     const std::string &pathKdMap = "",
	     const std::string &pathKsMap = "",
	     const std::string &pathNsMap = "",
	     const std::string &pathBumpMap = "");
  ~GlMaterial();

  inline bool hasKaMap() const {return _mapKa!=NULL;}
  inline bool hasKdMap() const {return _mapKd!=NULL;}
  inline bool hasKsMap() const {return _mapKs!=NULL;}
  inline bool hasNsMap() const {return _mapNs!=NULL;}
  inline bool hasBumpMap() const {return _mapBump!=NULL;}

  inline Vector3f ka() const {return _ka;}
  inline Vector3f kd() const {return _kd;}
  inline Vector3f ks() const {return _ks;}
  inline float    ns() const {return _ns;}
  
  inline FloatTexture2D *kaMap() {return _mapKa;}
  inline FloatTexture2D *kdMap() {return _mapKd;}
  inline FloatTexture2D *ksMap() {return _mapKs;}
  inline FloatTexture2D *nsMap() {return _mapNs;}
  inline FloatTexture2D *bumpMap() {return _mapBump;}

  
private:
  Vector3f _ka;
  Vector3f _kd;
  Vector3f _ks;
  float    _ns;

  FloatTexture2D *_mapKa;
  FloatTexture2D *_mapKd;
  FloatTexture2D *_mapKs;
  FloatTexture2D *_mapNs;
  FloatTexture2D *_mapBump;
  

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

class GlScene : public GlContext{
 public:
  GlScene(const std::string &filename);
  ~GlScene();

  inline unsigned int nbMeshes() const {return _subMeshIds.size();}
  inline void drawVAO(unsigned int id,bool useTesselation=false);

  inline const Vector3f barycenter() const {return _barycenter;}
  inline const float radius() const {return _radius;}
  inline GlMaterial *material(unsigned int id) {return _mats[_matIds[id]];}
  
 private:
  void createVAO(ObjMesh *m);
  void deleteVAO() {delete _vao; _vao = NULL;}
  void createIndexArray(ObjMesh *m,std::vector<Vector3i > &faces);
  void createGlMaterials(ObjMesh *m);
  void deleteGlMaterials();
  
  void computeSceneProperties(ObjMesh *m);
  
  VertexarrayObject        *_vao;
  std::vector<Vector2i>     _subMeshIds; // start-end indices for each sub mesh
  std::vector<int>          _matIds;
  std::vector<GlMaterial *> _mats;
  
  Vector3f _barycenter;
  float    _radius;
  
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

inline void GlScene::drawVAO(unsigned int id,bool useTesselation) {
  _vao->bind();

  if(useTesselation) {
    _glf->glPatchParameteri(GL_PATCH_VERTICES,3);
    _vao->drawElements(GL_PATCHES,_subMeshIds[id][1]-_subMeshIds[id][0]+1,_subMeshIds[id][0]);
  } else {
    _vao->drawElements(GL_TRIANGLES,_subMeshIds[id][1]-_subMeshIds[id][0]+1,_subMeshIds[id][0]);
  }
  
  _vao->unbind();
}

#endif // GL_SCENE
